package com.example.switchactivity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.switchactivity.databinding.ActivitySecondBinding;

public class SecondActivity extends AppCompatActivity {

    private ActivitySecondBinding activitySecondBinding;

    private String inputText;

    private String getText;

    public String getGetText() {
        return getText;
    }

    public String getInputText() {
        return inputText;
    }

    public void setInputText(String inputText) {
        this.inputText = inputText;
    }

    public void switchActivity(){
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("textSend",inputText);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        if(getIntent().getStringExtra("textSend")!=null) {
            getText = "Your text is \"" + getIntent().getStringExtra("textSend") + "\"";
        }else{
            getText = "Your text is \"\"";
        }
        activitySecondBinding = DataBindingUtil.setContentView(this,R.layout.activity_second);
        activitySecondBinding.setItemSecondActivity(this);
    }
}
